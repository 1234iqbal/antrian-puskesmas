<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Dokter;

class DokterController extends Controller
{
    public function index(){
        
        $dokter = dokter::get();
        return view('dokter.index', ['dokter' => $dokter]);
    }

    public function create(){
        return view('dokter.create');
    }

    public function store(Request $request) {
        $dokter = Dokter::create([
            'nama' => $request->nama,
            'jam' => $request->jam,
        ]);

        return redirect('/dokter');
    }

    public function edit($id){
        $dokter = dokter::find($id);

        return view('dokter.edit', [ 'dokter' => $dokter]);
    }

    public function update(Request $request, $id){
        $dokter = dokter::find($id);
        $dokter->nama = $request->nama;
        $dokter->jam = $request->jam;
        $dokter->save();

        return redirect(url('/dokter'));
    }

    public function delete($id){
        $dokter = dokter::find($id);
        $dokter->delete();

        return redirect(url('/dokter'));
    }
}
