<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\kategori_obat;

class KategoriObatController extends Controller
{
    public function index()
    {
        $kategoriobat = kategori_obat::get();
        return view('kategoriobat.index',['kategori_obats' => $kategoriobat]);
    }
/*
    public function tambah()
    {
 
	// memanggil view tambah
    return view('kategoriobat.tambah');
    }
    //menyimpan inputan 
    public function simpan(Request $request)
    {
        // insert data ke table pegawai
        ('kategori_obat')->insert([
            'id' => $request->id,
            'nama' => $request->nama
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/kategoriobat');
    
    }   */


    public function create(){
        return view("kategoriobat.create");
    }
    public function store(Request $request){
        $kategoriobat= kategori_obat::create([
            "nama" => $request->nama
        ]);
        return redirect(url ("/kategoriobat"));
    }
    public function edit($id){
        $kategoriobat = kategori_obat::find($id);

        return view("kategoriobat.edit",['kategoriobat'=>$kategoriobat]);
    }
    public function update(request $request, $id){
       $kategoriobat= kategori_obat::find($id);
       $kategoriobat->id=$request->id;
       $kategoriobat->nama=$request->nama;
       $kategoriobat->save();
        return redirect(url('/kategoriobat'));
    }
    public function delete($id){
        $kategoriobat = kategori_obat::find($id);
        $kategoriobat -> delete() ;
        return redirect (url ("/kategoriobat"));
    }
}
