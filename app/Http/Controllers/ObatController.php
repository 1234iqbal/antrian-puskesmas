<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Obat;

class ObatController extends Controller
{
    public function index(){
        
        $obat = obat::get();
        return view('obat.index', ['obat' => $obat]);
    }
    
    public function create(){
        return view('obat.create');
    }

    public function store(Request $request) {
        $obat = Obat::create([
            'nama' => $request->nama,
            'persediaan' => $request->persediaan,
            'id_kategori_obat' => $request->id_kategori_obat,
        ]);

        return redirect(url('/obat'));
    }

    public function edit($id){
        $obat = obat::find($id);

        return view('obat.edit', [ 'obat' => $obat]);
    }

    public function update(Request $request, $id){
        $obat = obat::find($id);
        $obat->nama = $request->nama;
        $obat->persediaan = $request->persediaan;
        $obat->id_kategori_obat = $request->id_kategori_obat;
        $obat->save();

        return redirect(url('/obat'));
    }

    public function delete($id){
        $obat = obat::find($id);
        $obat->delete();

        return redirect(url('/obat'));
    }
}
