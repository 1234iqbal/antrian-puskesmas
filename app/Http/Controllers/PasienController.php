<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pasien;

class PasienController extends Controller
{
    public function index(){
        
        $pasien = Pasien::get();
        return view('pasien.index', ['pasien' => $pasien]);
    }

    public function create(){
        return view('pasien.create');
    }

    public function store(Request $request) {
        $pasien = Pasien::create([
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'no_telepon' => $request->no_telepon,
        ]);

        return redirect(url('/pasien'));
    }

    public function edit($id){
        $pasien = pasien::find($id);

        return view('pasien.edit', [ 'pasien' => $pasien]);
    }

    public function update(Request $request, $id){
        $pasien = pasien::find($id);
        $pasien->nama = $request->nama;
        $pasien->alamat = $request->alamat;
        $pasien->no_telepon = $request->no_telepon;
        $pasien->save();

        return redirect(url('/pasien'));
    }

    public function delete($id){
        $pasien = pasien::find($id);
        $pasien->delete();

        return redirect(url('/pasien'));
    }
}
