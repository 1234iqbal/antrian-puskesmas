<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pemeriksaan;

class PemeriksaanController extends Controller
{
    public function index(){
        
        $pemeriksaan = pemeriksaan::get();
        return view('pemeriksaan.index', ['pemeriksaan' => $pemeriksaan]);
    }

    public function create(){

        return view('pemeriksaan.create');
    }

    public function store(Request $request) {
        $pemeriksaan = Pemeriksaan::create([
            'id_dokter' => $request->id_dokter,
            'no_pendaftaran' => $request->no_pendaftaran,
            'keterangan' => $request->keterangan,
        ]);

        return redirect(url('/pemeriksaan'));
    }

    public function edit($id){
        $pemeriksaan = pemeriksaan::find($id);

        return view('pemeriksaan.edit', [ 'pemeriksaan' => $pemeriksaan]);
    }

    public function update(Request $request, $id){
        $pemeriksaan = pemeriksaan::find($id);
        $pemeriksaan->id_dokter = $request->id_dokter;
        $pemeriksaan->no_pendaftaran = $request->no_pendaftaran;
        $pemeriksaan->keterangan = $request->keterangan;
        $pemeriksaan->save();

        return redirect(url('/pemeriksaan'));
    }

    public function delete($id){
        $pemeriksaan = pemeriksaan::find($id);
        $pemeriksaan->delete();

        return redirect(url('/pemeriksaan'));
    }
}
