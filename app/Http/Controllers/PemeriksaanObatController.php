<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pemeriksaan_Obat;

class PemeriksaanObatController extends Controller
    {
        public function index(){
            
            $pemeriksaanobat = pemeriksaan_obat::get();
            return view('pemeriksaanobat.index', ['pemeriksaanobat' => $pemeriksaanobat]);
        }
    }
