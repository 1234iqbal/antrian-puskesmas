<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pendaftaran;

class PendaftaranController extends Controller
{
    public function index(){
        
        $pendaftaran = pendaftaran::get();
        return view('pendaftaran.index', ['pendaftaran' => $pendaftaran]);
    }

    public function create(){
        return view('pendaftaran.create');
    }

    public function store(Request $request) {
        $pendaftaran = Pendaftaran::create([
            'id_pasien' => $request->id_pasien,
            'no_pendaftaran' => $request->no_pendaftaran,
            'keterangan' => $request->keterangan,
            'id_operator' => $request->id_operator,
        ]);

        return redirect(url('/pendaftaran'));
    }

    public function edit($id){
        $pendaftaran = pendaftaran::find($id);

        return view('pendaftaran.edit', [ 'pendaftaran' => $pendaftaran]);
    }

    public function update(Request $request, $id){
        $pendaftaran = Pendaftaran::findd($id);
        $pendaftaran->id_pasien = $request->id_pasien;
        $pendaftaran->no_pendaftaran = $request->no_pendaftaran;
        $pendaftaran->keterangan = $request->keterangan;
        $pendaftaran->id_operator = $request->id_operator;

        return redirect(url('/pendaftaran'));
    }

    public function delete($id){
        $pendaftaran = pendaftaran::find($id);
        $pendaftaran->delete();

        return redirect(url('/pendaftaran'));
    }
}
