<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Obat extends Model
{
    protected $table = "obats";
    protected $fillable = [
        'nama', 'persediaan', 'id_kategori_obat'
    ];

    public function kategori()
    {
        return $this->belongsTo('App\Models\kategori_obat');
    }
}
