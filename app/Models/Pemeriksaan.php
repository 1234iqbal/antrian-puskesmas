<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pemeriksaan extends Model
{
    protected $table = "pemeriksaans";
    protected $fillable = [
        'id_dokter', 'no_pendaftaran', 'keterangan'
    ];
}
