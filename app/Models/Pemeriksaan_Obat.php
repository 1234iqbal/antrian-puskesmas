<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pemeriksaan_Obat extends Model
{
    protected $table = "pemeriksaan_obats";
    protected $fillable = [
        'id_obat', 'id_pemeriksaan', 'no_pendaftaran', 'keterangan'
    ];
}
