<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pendaftaran extends Model
{
    protected $table = "pendaftarans";
    protected $fillable = [
        'id_pasien', 'no_pendaftaran', 'keterangan', 'id_operator'
    ];
}
