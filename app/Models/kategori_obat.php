<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class kategori_obat extends Model
{
    protected $table = "kategori_obats";
    protected $fillable = [
        'id', 'nama'
    ];

    public function obat()
    {
        return $this->hasMany('App\Models\Obat');
    }    
    
}
