<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(DokterSeeder::class);
        $this->call(Kategori_obatSeeder::class);
        $this->call(ObatSeeder::class);
        $this->call(PasienSeeder::class);
        $this->call(Pemeriksaan_obatSeeder::class);
        $this->call(PendaftaranSeeder::class);
    }
}
