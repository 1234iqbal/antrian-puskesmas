<?php

use Illuminate\Database\Seeder;

class ObatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('obats')->insert([
        	'nama' => 'amox',
            'persediaan' => '44',
            'id_kategori_obat' => '12'

        ]);
    }
}
