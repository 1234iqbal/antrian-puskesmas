<?php

use Illuminate\Database\Seeder;

class PasienSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        DB::table('pasiens')->insert([
        	'nama' => 'Joni',
        	'alamat' => 'ndoglik',
        	'no_telepon' => "082999222111"
        ]);
    }
}
