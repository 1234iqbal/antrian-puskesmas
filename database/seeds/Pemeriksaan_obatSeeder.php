<?php

use Illuminate\Database\Seeder;

class Pemeriksaan_obatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pemeriksaan_obats')->insert([
        	'id_obat' => '111',
            'id_pemeriksaan' => '123',
            'no_pendaftaran' => '44',
            'keterangan' => 'kepala'

        ]);
    }
}
