<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PendaftaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pendaftarans')->insert([
        	'id_pasien' => '111',
            'no_pendaftaran' => '44',
            'keterangan' => 'kepala',
            'id_operator' => '12'

        ]);
    }
}
