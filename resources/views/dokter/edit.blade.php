@extends('layout.master')

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
        <h3 class="box-title">Quick Example</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" method="post" action="{{url("/dokter/edit", $dokter->id)}}">
        {{ csrf_field() }}
        <div class="box-body">
            <div class="form-group">
                <label for="exampleInputEmail1">Nama</label>
                <input type="text" name="nama" class="form-control" id="exampleInputEmail1" value="{{ $dokter->nama }}" placeholder="Nama"> 
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Jam</label>
                <input type="text" name="jam" class="form-control" id="exampleInputEmail1" value="{{ $dokter->jam }}"placeholder="Jam">
            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
    </div>
@endsection