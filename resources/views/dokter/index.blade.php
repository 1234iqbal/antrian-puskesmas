@extends('layout.master')

@section('content')
<!-- /.card-header -->
<div class="card-body">
  <table id="data-table" class="table table-bordered table-hover">
      <thead>
        <a href="{{url('/dokter/create')}}"> 
          <button type="button" class="btn btn-primary">Tambah</button>
        </a>
      <tr>
        <th>Id</th>
        <th>Nama</th>
        <th>Jam</th>
        <th>Action</th>
      </tr>
      </thead>
      <tbody>
        <?php $i = 1; ?>
            @foreach($dokter as $dokter)
            <tr>
                <td class="text-center">{{$dokter->id}}</td>
                <td>{{$dokter->nama}}</td>
                <td>{{$dokter->jam}}</td>
                <td>
                  <a href="{{url('/dokter/edit', $dokter->id) }}">
                    <button type="button" class="btn btn-primary btn-xs">Edit</button>
                  </a>  | 
                  <a href="{{url('/dokter/delete', $dokter->id) }}">
                    <button type="button" class="btn btn-xs btn-danger">Delete</button>
                </td>
            </tr>
            @endforeach
      </tbody>
      </table>
  </div>
  <!-- /.card-body -->

@endsection