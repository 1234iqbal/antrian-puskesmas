@extends('layout.master')
@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Tambah Kategori Obat</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form method="POST" action="{{url("/kategoriobat/create")}}" role="form">
        {{ csrf_field() }}
      <div class="box-body">
        <div class="form-group">
            <label for="exampleInputEmail1">Nama Obat</label>
            <input type="text" name="nama" class="form-control" id="exampleInputEmail1" placeholder="Enter Nama">
        </div>
      </div>
      <!-- /.box-body -->

      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        <a href="{{url("/kategoriobat")}}">
            <button type="button" class="btn btn-primary">Back</button>
        </a>
      </div>

    </form>
  </div>
    
@endsection