@extends('layout.master')
@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Edit Kategori Obat</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form action="{{url ('/kategoriobat/edit', $kategoriobat->id)}}" method="post" role="form"> 
      {{ csrf_field() }}
      <div class="box-body">
        <div class="form-group">
          <label for="exampleInputEmail1">ID Obat</label>
          <input type="text" class="form-control" name="id" id="exampleInputEmail1" value='{{$kategoriobat->id}}' placeholder="Enter ID">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Nama Obat</label>
            <input type="text" class="form-control" name="nama" id="exampleInputEmail1" value='{{$kategoriobat->nama}}' placeholder="Enter Nama">
        </div>
      </div>
      <!-- /.box-body -->

      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
    
@endsection