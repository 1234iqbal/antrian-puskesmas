@extends('layout.master')
@section('content')
<a href="{{url("/kategoriobat/create")}}">
<button type="button" class="btn btn-block btn-primary">Tambah Kategori</button>
</a>

              <!-- /.card-header -->
              <div class="card-body">
                <table id="data-table" class="table table-bordered table-hover">  <!-- data-table itu refer ke script.blade nya-->
                  <thead>
                  <tr>
                    <th>ID</th>
                    <th>Nama</th>
                    <th>option</th>


                  </tr>
                  </thead>
                  <tbody>
					@foreach($kategori_obats as $kategori_obats)
					<tr>
						<td>{{ $kategori_obats->id }}</td>
						<td>{{ $kategori_obats->nama }}</td>
						<td>
              <a href="{{url ('/kategoriobat/edit',$kategori_obats->id) }}">
                <button type="button" class="btn btn-primary">Edit</button>
              </a>
              |
              <a href="{{url('/kategoriobat/delete', $kategori_obats->id) }}">
                <button type="button" class="btn btn-primary">Hapus</button>
              </a>
						</td>
					</tr>
					@endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
	
@endsection