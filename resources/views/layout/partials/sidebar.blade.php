<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Alexander Pierce</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
          <a href="{{ url('/dashboard')}}" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview ">
          <a href="{{ url('/obat')}}" class="nav-link ">
              <i class=""></i>
              <p>
                Obat
                <i class="right fas fa-angle-right"></i>
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview ">
            <a href="{{ url('/dokter')}}" class="nav-link ">
                <i class=""></i>
                <p>
                  Dokter
                  <i class="right fas fa-angle-right"></i>
                </p>
              </a>
          </li>
            <li class="nav-item has-treeview ">
                <a href="{{ url('/pasien')}}" class="nav-link ">
                    <i class=""></i>
                    <p>
                      Pasien
                    <i class="right fas fa-angle-right"></i>
                </p>
              </a>
            </li>

          <li class="nav-item has-treeview">
            <a href="{{ url('/kategoriobat')}}" class="nav-link">
              <p>
                kategori obat
                <i class="right fas fa-angle-right"></i>
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview ">
            <a href="{{ url('/pendaftaran')}}" class="nav-link ">
                <i class=""></i>
                <p>
                  Pendaftaran
                <i class="right fas fa-angle-right"></i>
            </p>
          </a>
        </li>

        <li class="nav-item has-treeview ">
          <a href="{{ url('/pemeriksaan')}}" class="nav-link ">
              <i class=""></i>
              <p>
                Pemeriksaan
              <i class="right fas fa-angle-right"></i>
          </p>
        </a>
      </li>

      <li class="nav-item has-treeview ">
        <a href="{{ url('/pemeriksaanobat')}}" class="nav-link ">
            <i class=""></i>
            <p>
              Pemeriksaan Obat
            <i class="right fas fa-angle-right"></i>
        </p>
      </a>
    </li>

      <li class="nav-item has-treeview ">
        <a href="{{ route('logout') }}"
        onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();" class="nav-link">
            <p>
              Logout
            <i class="right fas fa-angle-right"></i>
            </p>
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
        </form>
      </li>

      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>