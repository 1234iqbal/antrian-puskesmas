@extends('layout.master')

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
        <h3 class="box-title">Quick Example</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" method="post" action="{{url("/obat/create")}}">
        {{ csrf_field() }}
        <div class="box-body">
            <div class="form-group">
                <label for="exampleInputEmail1">Nama</label>
                <input type="text" name="nama" class="form-control" id="exampleInputEmail1" placeholder="Nama">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Persediaan</label>
                <input type="text" name="persediaan" class="form-control" id="exampleInputEmail1" placeholder="Persediaan">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">ID Kategori Obat</label>
                <input type="text" name="id_kategori_obat" class="form-control" id="exampleInputEmail1" placeholder="ID Kategori Obat">
            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="{{url('/obat')}}">
          <button type="button" class="btn btn-warning">Back</button>
        </a>
        </div>
        </form>
    </div>
@endsection