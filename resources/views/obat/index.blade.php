@extends('layout.master')

@section('content')
<a href="{{url('/obat/create')}}"> 
  <button type="button" class="btn btn-primary">Tambah</button>
</a>
<!-- /.card-header -->
<div class="card-body">
    <table id="data-table" class="table table-bordered table-hover">
      <thead>
      <tr>
        <th>Id</th>
        <th>Nama</th>
        <th>Persediaan</th>
        <th>ID Kategori Obat</th>
        <th>Action</th>
      </tr>
      </thead>
      <tbody>
        <?php $i = 1; ?>
            @foreach($obat as $obat)
            <tr>
                <td class="text-center">{{$i++}}</td>
                <td>{{$obat->nama}}</td>
                <td>{{$obat->persediaan}}</td>
                <td>{{$obat->id_kategori_obat}}</td>
                <td>
                <a href="{{url('/obat/edit', $obat->id) }}">
                  <button type="button" class="btn btn-primary btn-xs">Edit</button>
                </a> 
                <a href="{{url('/obat/delete', $obat->id) }}">
                  <button type="button" class="btn btn-xs btn-danger">Delete</button>
                </a>
                </td>
            </tr>
            @endforeach
      </tbody>
    </table>
  </div>
  <!-- /.card-body -->

@endsection