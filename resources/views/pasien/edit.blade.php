@extends('layout.master')

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
        <h3 class="box-title">Quick Example</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" method="post" action="{{url("/pasien/edit", $pasien->id)}}">
        {{ csrf_field() }}
        <div class="box-body">
            <div class="form-group">
                <label for="exampleInputEmail1">Nama</label>
                <input type="text" name="nama" class="form-control" id="exampleInputEmail1" value="{{ $pasien->nama }}" placeholder="Nama"> 
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Alamat</label>
                <input type="text" name="alamat" class="form-control" id="exampleInputEmail1" value="{{ $pasien->alamat }}"placeholder="Alamat">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">No Telepon</label>
                <input type="text" name="no_telepon" class="form-control" id="exampleInputEmail1" value="{{ $pasien->no_telepon }}"placeholder="No Telepon">
            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
    </div>
@endsection