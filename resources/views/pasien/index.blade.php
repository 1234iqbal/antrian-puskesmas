@extends('layout.master')

@section('content')
<!-- /.card-header -->
<div class="card-body">
    <table id="data-table" class="table table-bordered table-hover">
      <thead>
        <a href="{{url('/pasien/create')}}"> 
          <button type="button" class="btn btn-primary">Tambah</button>
        </a>
      <tr>
        <th>Id</th>
        <th>Nama</th>
        <th>Alamat</th>
        <th>No Telepon</th>
        <th>Action</th>
      </tr>
      </thead>
      <tbody>
        <?php $i = 1; ?>
            @foreach($pasien as $pasien)
            <tr>
                <td class="text-center">{{$pasien->id}}</td>
                <td>{{$pasien->nama}}</td>
                <td>{{$pasien->alamat}}</td>
                <td>{{$pasien->no_telepon}}</td>
                <td>
                  <a href="{{url('/pasien/edit', $pasien->id) }}">
                    <button type="button" class="btn btn-primary btn-xs">Edit</button>
                  </a> 
                  <a href="{{url('/pasien/delete', $pasien->id) }}">
                    <button type="button" class="btn btn-xs btn-danger">Delete</button>
                  </a>
                </td>
            </tr>
            @endforeach
      </tbody>
      </table>
  </div>
  <!-- /.card-body -->

@endsection