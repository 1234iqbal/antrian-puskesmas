@extends('layout.master')

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
    <h3 class="box-title">Quick Example</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form role="form" method="post" action="{{url("/pemeriksaan/edit", $pemeriksaan->id)}}">
    {{ csrf_field() }}
    <div class="box-body">
        <div class="form-group">
            <label for="exampleInputEmail1">ID Dokter</label>
            <input type="text" name="id_dokter" class="form-control" id="exampleInputEmail1" placeholder="Id Dokter">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">No Pendaftaran</label>
            <input type="text" name="no_pendaftaran" class="form-control" id="exampleInputEmail1" placeholder="No Pendaftaran">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Keterangan</label>
            <input type="text" name="keterangan" class="form-control" id="exampleInputEmail1" placeholder="Keterangan">
        </div>
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
    </form>
</div>
@endsection