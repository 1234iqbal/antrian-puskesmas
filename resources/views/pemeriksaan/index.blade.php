@extends('layout.master')

@section('content')
<!-- /.card-header -->
<div class="card-body">
  <table id="data-table" class="table table-bordered table-hover">
    <thead>
      <a href="{{url('/pemeriksaan/create')}}"> 
        <button type="button" class="btn btn-primary">Tambah</button>
      </a>
      <tr>
        <th>Id</th>
        <th>Id Dokter</th>
        <th>No Pendaftaran</th>
        <th>Keterangan</th>
        <th>Action</th>
      </tr>
      </thead>
      <tbody>
        <?php $i = 1; ?>
            @foreach($pemeriksaan as $pemeriksaan)
            <tr>
                <td class="text-center">{{$pemeriksaan->id}}</td>
                <td>{{$pemeriksaan->id_dokter}}</td>
                <td>{{$pemeriksaan->no_pendaftaran}}</td>
                <td>{{$pemeriksaan->keterangan}}</td>
                <td>
                <a href="{{url('/pemeriksaan/edit', $pemeriksaan->id) }}">
                  <button type="button" class="btn btn-primary btn-xs">Edit</button>
                </a> 
                <a href="{{url('/pemeriksaan/delete', $pemeriksaan->id) }}">
                  <button type="button" class="btn btn-xs btn-danger">Delete</button>
                </a>
                </td>
            </tr>
            @endforeach
      </tbody>
      </table>
  </div>
  <!-- /.card-body -->

@endsection