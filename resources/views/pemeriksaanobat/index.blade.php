@extends('layout.master')

@section('content')
<!-- /.card-header -->
<div class="card-body">
    <table id="data-table" class="table table-bordered table-hover">
      <thead>
      <tr>
        <th>Id</th>
        <th>Id Obat</th>
        <th>Id Pemeriksaan</th>
        <th>No Pendaftaran</th>
        <th>Keterangan</th>
      </tr>
      </thead>
      <tbody>
        <?php $i = 1; ?>
            @foreach($pemeriksaanobat as $pemeriksaanobat)
            <tr>
                <td class="text-center">{{$pemeriksaanobat->id}}</td>
                <td>{{$pemeriksaanobat->id_obat}}</td>
                <td>{{$pemeriksaanobat->id_pemeriksaan}}</td>
                <td>{{$pemeriksaanobat->no_pendaftaran}}</td>
                <td>{{$pemeriksaanobat->keterangan}}</td>
                <td>
                    <a href="{{url('/edit')}" class="btn btn-xs btn-primary">Edit</a>| 
                    <a href="#" class="btn btn-xs btn-danger" onclick="return confirm('yakin?');">Delete</a>  
                </td>
            </tr>
            @endforeach
      </tbody>
      </table>
  </div>
  <!-- /.card-body -->

@endsection