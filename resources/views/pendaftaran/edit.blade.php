@extends('layout.master')

@section('name')
<div class="box box-primary">
    <div class="box-header with-border">
    <h3 class="box-title">Quick Example</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form role="form" method="post" action="{{url("/pendaftaran/edit", $pendaftaran->id")}}">
    {{ csrf_field() }}
    <div class="box-body">
        <div class="form-group">
            <label for="exampleInputEmail1">Id Pasien</label>
            <input type="text" name="id_pasien" class="form-control" id="exampleInputEmail1" placeholder="ID Pasien">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">No Pendaftaran</label>
            <input type="text" name="no_pendaftaran" class="form-control" id="exampleInputEmail1" placeholder="No Pendaftaran">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Keterangan</label>
            <input type="text" name="keterangan" class="form-control" id="exampleInputEmail1" placeholder="Keterangan">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Id Operator</label>
            <input type="text" name="id_operator" class="form-control" id="exampleInputEmail1" placeholder="Id Operator">
        </div>
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
    </form>
</div>
@endsection