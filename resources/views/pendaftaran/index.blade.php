@extends('layout.master')

@section('content')
<a href="{{url('/pendaftaran/create')}}"> 
  <button type="button" class="btn btn-primary">Tambah</button>
</a>
<!-- /.card-header -->
<div class="card-body">
    <table id="data-table" class="table table-bordered table-hover">
      <thead> 
      <tr>
        <th>Id</th>
        <th>Id Pasien</th>
        <th>No Pendaftaran</th>
        <th>Keterangan</th>
        <th>Id Operator</th>
        <th>Action</th>
      </tr>
      </thead>
      <tbody>
        <?php $i = 1; ?>
            @foreach($pendaftaran as $pendaftaran)
            <tr>
                <td class="text-center">{{$pendaftaran->id}}</td>
                <td>{{$pendaftaran->id_pasien}}</td>
                <td>{{$pendaftaran->no_pendaftaran}}</td>
                <td>{{$pendaftaran->keterangan}}</td>
                <td>{{$pendaftaran->id_operator}}</td>
                <td>
                  <a href="{{url('/pendaftaran/edit', $pendaftaran->id) }}">
                    <button type="button" class="btn btn-primary btn-xs">Edit</button>
                  </a> 
                  <a href="{{url('/pendaftaran/delete', $pendaftaran->id) }}">
                    <button type="button" class="btn btn-xs btn-danger">Delete</button>
                  </a>
                </td>
            </tr>
            @endforeach
      </tbody>
      </table>
  </div>
  <!-- /.card-body -->

@endsection