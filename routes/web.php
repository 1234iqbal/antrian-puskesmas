<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
|--------------------------------------------------------------------------
| Login Register
|--------------------------------------------------------------------------
|
*/

Route::get('/', function() {
    return redirect('/login');
});

Auth::routes();

route::group(['middleware' => ['auth']], function() {

            /*
            |--------------------------------------------------------------------------
            | DASHBOARD
            |--------------------------------------------------------------------------
            |
            */

            Route::get('/dashboard', 'DashboardController@index');

            /*
            |--------------------------------------------------------------------------
            | CRUD OBAT
            |--------------------------------------------------------------------------
            |
            */

            Route::get('/obat', 'ObatController@index');
            Route::get('/obat/create', 'ObatController@create');
            Route::post('/obat/create', 'ObatController@store');
            Route::get('/obat/edit/{id}', 'ObatController@edit');
            Route::post('/obat/edit/{id}', 'ObatController@update');
            Route::get('/obat/delete/{id}', 'ObatController@delete');

            /*
            |--------------------------------------------------------------------------
            | CRUD DOKTER
            |--------------------------------------------------------------------------
            |
            */

            Route::get('/dokter', 'DokterController@index');
            Route::get('/dokter/create', 'DokterController@create');
            Route::post('/dokter/create', 'DokterController@store');
            Route::get('/dokter/edit/{id}', 'DokterController@edit');
            Route::post('/dokter/edit/{id}', 'DokterController@update');
            Route::get('/dokter/delete/{id}', 'DokterController@delete');

            /*
            |--------------------------------------------------------------------------
            | CRUD KATEGORI OBAT
            |--------------------------------------------------------------------------
            |
            */

            Route::get('/kategoriobat', 'KategoriObatController@index');
            Route::get('/kategoriobat/create', 'KategoriObatController@create');
            Route::post('/kategoriobat/create', 'KategoriObatController@store');
            Route::get('/kategoriobat/edit/{id}', 'KategoriObatController@edit');
            Route::post('/kategoriobat/edit/{id}', 'KategoriObatController@update');
            Route::get('/kategoriobat/delete/{id}', 'KategoriObatController@delete');

            /*
            |--------------------------------------------------------------------------
            | CRUD PASIEN
            |--------------------------------------------------------------------------
            |
            */

            Route::get('/pasien', 'PasienController@index');
            Route::get('/pasien/create', 'PasienController@create');
            Route::post('/pasien/create', 'PasienController@store');
            Route::get('/pasien/edit/{id}', 'PasienController@edit');
            Route::post('/pasien/edit/{id}', 'PasienController@update');
            Route::get('/pasien/delete/{id}', 'PasienController@delete');

            /*
            |--------------------------------------------------------------------------
            | CRUD PENDAFTARAN
            |--------------------------------------------------------------------------
            |
            */

            Route::get('/pendaftaran', 'PendaftaranController@index');
            Route::get('/pendaftaran/create', 'PendaftaranController@create');
            Route::post('/pendaftaran/create', 'PendaftaranController@store');
            Route::get('/pendaftaran/edit/{id}', 'PendaftaranController@edit');
            Route::post('/pendaftaran/edit/{id}', 'PendaftaranController@update');
            Route::get('/pendaftaran/delete/{id}', 'PendaftaranController@delete');

            /*
            |--------------------------------------------------------------------------
            | CRUD PEMERIKSAAN
            |--------------------------------------------------------------------------
            |
            */

            Route::get('/pemeriksaan', 'PemeriksaanController@index');
            Route::get('/pemeriksaan/create', 'PemeriksaanController@create');
            Route::post('/pemeriksaan/create', 'PemeriksaanController@store');
            Route::get('/pemeriksaan/edit/{id}', 'PemeriksaanController@edit');
            Route::post('/pemeriksaan/edit/{id}', 'PemeriksaanController@update');
            Route::get('/pemeriksaan/delete/{id}', 'PemeriksaanController@delete');

            /*
            |--------------------------------------------------------------------------
            | CRUD PEMERIKSAAN OBAT
            |--------------------------------------------------------------------------
            |
            */

            Route::get('/pemeriksaanobat', 'PemeriksaanObatController@index');
            Route::get('/pemeriksaanobat/create', 'PemeriksaanObatController@create');
            Route::post('/pemeriksaanobat/create', 'PemeriksaanObatController@store');
            Route::get('/pemeriksaanobat/edit/{id}', 'PemeriksaanObatController@edit');
            Route::post('/pemeriksaanobat/edit/{id}', 'PemeriksaanObatController@update');
            Route::get('/pemeriksaanobat/delete/{id}', 'PemeriksaanObatController@delete');

});